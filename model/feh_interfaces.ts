
// The full FEH character interface with all data correctly aligned

interface feh_character {
    id_tag: string;
    id_num: number;
    names: string[];
    roman: string;
    face_name: string;
    detail: {
        weapon_type: string;
        move_type: string;
        legendary?: {
            duo_skill_id: string | null;
            bonus_effect: {
                hp: number;
                atk: number;
                spd: number;
                def: number;
                res: number;
            };
            kind: number;
            element: number;
            bst: number;
            pair_up: boolean
        };
        refresher: boolean;
        dragonflowers: {
            max_count: number;
        };
        series: number;
        alt_base: string;
    };
    summon_pool: {
        regular_hero: boolean;
        permanent_hero: boolean;
        seasonal_hero: boolean;
        S5: number;
        S4: number;
        S3: number;
    };
    ivs: ivs;
    skills: character_skills;
    localization: {
        DE: character_strings;
        EN: character_strings;
    };
}

// The full FEH skill interface with all data correctly aligned

interface feh_skill {
    id_tag : string;
    id_num : number;
    category : string;
    wep_equip : string[];
    mov_equip : string[];
    exclusive : boolean;
    enemy_only : boolean;
    sp_cost : number;
    icon_id : number;
    detail : {
        might : number;
        range : number;
        stats : number,
        class_params : stat_object;
        skill_params : stat_object;
        effectivness : {
            wep_effective: string[];
            mov_effective: string[];
            wep_shield: string[];
            mov_shield: string[];
            wep_weakness: string[];
            mov_weakness: string[];
            wep_adaptive: string[];
            mov_adaptive: string[];
        };
        sprites : string[];
        prerequisites : string[];
        next_skill : string;
        passive_next : string;
    };
    effect : {
        cooldown_count : number;
        assist_cd : boolean;
        healing : boolean;
        skill_range : number;
        timing_id : number;
        ability_id : number;
        limit1_id : number;
        limit1_params : number[];
        limit2_id : number;
        limit2_params : number[];
        target_wep : string[];
        target_mov : string[];
    };
    refine : {
        refined : boolean;
        refine_base : string;
        refine_id : number;
        refine_stats : stat_object;
        sort_id : number;
    };
    localization : {
        DE : skill_strings;
        EN : skill_strings;
    };
}

interface basestats {
    S5: stat_object;
    S4: stat_object;
    S3: stat_object;
}

interface ivs {
    S5?: iv_object;
    S4?: iv_object;
    S3?: iv_object;
}

interface iv_object {
    hp: iv_stats;
    atk: iv_stats;
    spd: iv_stats;
    def: iv_stats;
    res: iv_stats;
}

interface iv_stats {
    LV01: number;
    LV40: number;
    bane?: boolean;
    boon?: boolean;
}

interface stat_object {
    hp: number;
    atk: number,
    spd: number;
    def: number;
    res: number;
}

interface character_skills {
    weapon: skill_object[];
    assist: skill_object[];
    special: skill_object[];
    passive_A: skill_object[];
    passive_B: skill_object[];
    passive_C: skill_object[];
}

interface skill_object {
    SID: string;
    default?: string;
    unlock?: string;
}

interface character_strings {
    name: string;
    title: string;
    description: string;
    voice: string;
    artist: string;
}

interface skill_strings {
    name: string;
    description: string;
}
