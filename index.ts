import rp = require('request-promise');
import bitwise = require('bitwise');

const {MongoClient} = require('mongodb');

const assetsPath = 'https://raw.githubusercontent.com/HertzDevil/feh-assets-json/master/files/assets/';

class FehExporter {

	data_file = "00_first.json";

	languages : {
	    [key: string]: any;
	} = {
		"EUDE" : {},
		"USEN" : {}
	};

	client: any;
	db: any;

	constructor(con: mongo_connection) {
		const uri = `mongodb://${con.username}:${con.password}@${con.cluster}/?authSource=admin&readPreference=primary`;
		this.initMongo(uri, con.database);
	}

	async initMongo(uri: string, database: string) {
		this.client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
		await this.client.connect();

		this.db = this.client.db(database);
	}

	// Main function. Compiles data to /json
	public async compile() {
		const filenames = await FehExporter.get_files('Common/SRPG/Person');

		for (let i=0; i < filenames.length; i++) {

			// This must be edited whenever a new book releases
			switch (filenames[i]) {
				case 'Tutorial.json':
				case '00_first.json':
				case '41_muspell01.json':
				case '71_hell01.json':
					this.data_file = '00_first.json';
					break;
				default:
					this.data_file = filenames[i];
					break;
			}

			//await this.compile_character_data(filenames[i]);
			//await this.compile_skill_data(filenames[i]);

		}

		await this.fetch_refines();
	}

	async fetch_refines() {

		const filenames = await FehExporter.get_files('Common/SRPG/WeaponRefine');
		const db_collection = this.db.collection('feh_refines');

		for (let i=0; i < filenames.length; i++) {

			let options = {
				uri: assetsPath + 'Common/SRPG/WeaponRefine/' + filenames[i],
				json: true
			};

			let output = await rp(options);

			for(const document of output) {
				await db_collection.updateOne(
					{ refined: document.refined },
					{ $set: document},
					{ upsert: true}
				);
			}


			//fs.writeFileSync("json/refines/" + filenames[i], output);
			console.log(`Exported 'refines/${filenames[i]}'.`);
		}

	}

	// Compiles skill data for MongoDB
	async compile_skill_data(filename: string) {

		let options = {
			uri: assetsPath + 'Common/SRPG/Skill/' + filename,
			json: true
		};

		let json = await rp(options);

		const db_collection = this.db.collection('feh_skills');
		//let collection = [];

		for(let i=0;i<json.length;i++) {

			let current = json[i];

			let new_obj: feh_skill = {
				"id_tag" : current.id_tag,
				"id_num" : current.id_num,
				"category" : skill_category(current.category),
				"wep_equip" : weapon_array(current.wep_equip),
				"mov_equip" : move_array(current.mov_equip),
				"exclusive" : current.exclusive,
				"enemy_only" : current.enemy_only,
				"sp_cost" : current.sp_cost,
				"icon_id" : current.icon_id,
				"detail" : {
					"might" : current.might,
					"range" : current.range,
					"stats" : current.stats,
					"class_params" : current.class_params,
					"skill_params" : current.skill_params,
					"effectivness" : {
						"wep_effective": weapon_array(current.wep_effective),
						"mov_effective": move_array(current.mov_effective),
						"wep_shield": weapon_array(current.wep_shield),
						"mov_shield": move_array(current.mov_shield),
						"wep_weakness": weapon_array(current.wep_weakness),
						"mov_weakness": move_array(current.mov_weakness),
						"wep_adaptive": weapon_array(current.wep_adaptive),
						"mov_adaptive": move_array(current.mov_adaptive),
					},
					"sprites" : current.sprites,
					"prerequisites" : current.prerequisites,
					"next_skill" : current.next_skill,
					"passive_next" : current.passive_next
				},
				"effect" : {
					"cooldown_count" : current.cooldown_count,
					"assist_cd" : current.assist_cd,
					"healing" : current.healing,
					"skill_range" : current.skill_range,
					"timing_id" : current.timing_id,
					"ability_id" : current.ability_id,
					"limit1_id" : current.limit1_id,
					"limit1_params" : current.limit1_params,
					"limit2_id" : current.limit2_id,
					"limit2_params" : current.limit2_params,
					"target_wep" : weapon_array(current.target_wep),
					"target_mov" : move_array(current.target_mov)
				},
				"refine" : {
					"refined" : current.refined,
					"refine_base" : current.refine_base,
					"refine_id" : current.refine_id,
					"refine_stats" : current.refine_stats,
					"sort_id" : current.refine_sort_id
				},
				"localization" : {
					"DE" : await this.localize_skill(current, "EUDE"),
					"EN" : await this.localize_skill(current, "USEN")
				}
			};

			//collection.push(new_obj);
			await db_collection.updateOne(
				{ id_tag: new_obj.id_tag },
				{ $set: new_obj},
				{ upsert: true}
			);

		}

		function weapon_array (bitmask: number) {
			let wpn_arr = [];
			for (let bit=0; bit < 32; bit++) {
				if (bitwise.integer.getBit(bitmask, bit) === 1) {
					wpn_arr.push(FehHelper.weaponString(bit));
				}
			}
			return wpn_arr;
		}

		function move_array (bitmask: number) {
			let mov_arr = [];
			for (let bit=0; bit < 32; bit++) {
				if (bitwise.integer.getBit(bitmask, bit) === 1) {
					mov_arr.push(FehHelper.moveString(bit));
				}
			}
			return mov_arr;
		}

		function skill_category(category: number) {

			let cat = "Weapon";

			switch (category) {
				case 1:
					cat = "Assist";
					break;
				case 2:
					cat = "Special";
					break;
				case 3:
					cat = "Passive_A";
					break;
				case 4:
					cat = "Passive_B";
					break;
				case 5:
					cat = "Passive_C";
					break;
				case 6:
					cat = "Seal";
					break;
				case 7:
					cat = "Refine";
					break;
				case 8:
					cat = "Transform";
					break;
			}

			return cat;

		}

		//const output = JSON.stringify(collection);
		//fs.writeFileSync("json/skills/" + filename, output);

		console.log(`Exported 'skills/${filename}' with a collection of ${json.length} items.`);

	}

	// Compiles character data for MongoDB
	async compile_character_data(filename: string) {

		let options = {
			uri: assetsPath + 'Common/SRPG/Person/' + filename,
			json: true
		};

		let json = await rp(options);

		const db_collection = this.db.collection('feh_heroes');
		//let collection = [];

		for(let i=0;i<json.length;i++) {

			let current = json[i];

			let base_stats: basestats = {
				"S3" : current.base_stats,
				"S4" : FehHelper.getBases(current.base_stats, "S4"),
				"S5" : FehHelper.getBases(current.base_stats, "S5")
			};

			let alt_base = /([A-Z]+(_[MF])?).*/g.exec(current.roman);

			let new_obj: feh_character = {
				"id_tag" : current.id_tag,
				"id_num": current.id_num,
				"names" : [],
				"roman" : current.roman,
				"face_name" : current.face_name,
				"detail" : {
					"weapon_type" : FehHelper.weaponString(current.weapon_type),
					"move_type" : FehHelper.moveString(current.move_type),
					"legendary" : current.legendary,
					"refresher" : current.refresher,
					"dragonflowers" : current.dragonflowers,
					"series" : current.series,
					"alt_base" : (alt_base) ? alt_base[1] : current.roman
				},
				"summon_pool" : {
					"regular_hero" : current.regular_hero,
					"permanent_hero" : current.permanent_hero,
					"seasonal_hero": false,
					"S5" : 1,
					"S4" : 0,
					"S3" : 0
				},
				"ivs" : FehHelper.calcIV(base_stats, current.growth_rates),
				"skills" : FehExporter.get_skills(current.skills),
				"localization" : {
					"DE" : await this.localize_char(current.id_tag, "EUDE"),
					"EN" : await this.localize_char(current.id_tag, "USEN")
				}
			};

			new_obj = this.finalize(new_obj);

			await db_collection.updateOne(
				{ id_tag: new_obj.id_tag },
				{ $set: new_obj},
				{ upsert: true}
			);

		}

		//const output = JSON.stringify(collection);
		//fs.writeFileSync("json/characters/" + filename, output);

		console.log(`Exported 'characters/${filename}' with a collection of ${json.length} items.`);

	}

	// Adds additional data to the collection
	finalize(character: feh_character) {

		let name_EN = "NONE";
		let name_DE = "NONE";

		try {
			name_EN = character.localization.EN.name.toLowerCase().replace(/\s/g, "_");
			name_DE = character.localization.DE.name.toLowerCase().replace(/\s/g, "_");
		}catch {
			console.error("Undefined name!");
		}

		const result = /(.+_)*?([A-Z]+)(_[MF])?(_[A-Z]+)?/g.exec(character.roman);

		if (result) {
            character.names = get_names(character, result, name_EN);

            if (name_EN !== name_DE) {
                character.names = character.names.concat(get_names(character, result, name_DE));
            }
        }

		function get_names(character: feh_character, regEx: RegExpExecArray, name: string) {

			let names = [];
			let gender = "";
			let special_tag = "";

			let seasonal = false;

			if (regEx[4] !== undefined) {
				special_tag = regEx[4].substr(1);
			}

			if (regEx[3] !== undefined) {
				gender = regEx[3].toLowerCase();
				name += gender;
			}

			switch(special_tag) {
				case "GOD":
					names.push(`mythic_${name}`);
					names.push(`M!${name}`);
					break;
				case "LEGEND":
					names.push(`legendary_${name}`);
					names.push(`L!${name}`);
					break;
				case "PAIR":
					names.push(`duo_${name}`);
					names.push(`pair_${name}`);
					names.push(`D!${name}`);
					break;
				case "POPULARITY":
					names.push(`brave_${name}`);
					names.push(`BR!${name}`);
					break;
				case "DARK":
					names.push(`fallen_${name}`);
					names.push(`dark_${name}`);
					names.push(`FA!${name}`);
					break;
				case "BON":
					names.push(`festival_${name}`);
					names.push(`FE!${name}`);
					seasonal = true;
					break;
				case "MIKATA":
					names.push(`kimono_${name}`);
					names.push(`mikata_${name}`);
					names.push(`MI!${name}`);
					seasonal = true;
					break;
				case "DREAM":
					names.push(`dream_${name}`);
					names.push(`adrift_${name}`);
					names.push(`AD!${name}`);
					break;
				case "ONSEN":
					names.push(`hotspring_${name}`);
					names.push(`onsen_${name}`);
					names.push(`HS!${name}`);
					seasonal = true;
					break;
				case "PICNIC":
					names.push(`picnic_${name}`);
					names.push(`PI!${name}`);
					seasonal = true;
					break;
				case "ECHOES":
					names.push(`echoes_${name}`);
					break;
				case "NEWYEAR":
					names.push(`newyear_${name}`);
					names.push(`NY!${name}`);
					seasonal = true;
					break;
				case "WINTER":
					names.push(`christmas_${name}`);
					names.push(`winter_${name}`);
					names.push(`WI!${name}`);
					seasonal = true;
					break;
				case "HALLOWEEN":
					names.push(`halloween_${name}`);
					names.push(`HA!${name}`);
					seasonal = true;
					break;
				case "SUMMER":
					names.push(`summer_${name}`);
					names.push(`SU!${name}`);
					seasonal = true;
					break;
				case "BRIDE":
					names.push(`wedding_${name}`);
					names.push(`bride_${name}`);
					names.push(`WE!${name}`);
					seasonal = true;
					break;
				case "SPRING":
					names.push(`spring_${name}`);
					names.push(`SP!${name}`);
					seasonal = true;
					break;
				case "VALENTINE":
					names.push(`valentine_${name}`);
					names.push(`love_${name}`);
					names.push(`VA!${name}`);
					seasonal = true;
					break;
				case "DANCE":
					names.push(`dance_${name}`);
					names.push(`PA!${name}`);
					seasonal = true;
					break;
				default:
					names.push(name);
					break;
			}

			if (seasonal) {
				character.summon_pool.seasonal_hero = true;
				character.summon_pool.S5 = 0;
			}

			if (character.detail.legendary) {
				if (character.detail.legendary.duo_skill_id == null) {
					character.summon_pool.S5 = 0;
				}
			}

			return names;

		}

		return character;

	}

	// Returns directory contents from Person
	static async get_files(path: string) {

		let options = {
			uri: 'https://api.github.com/repos/HertzDevil/feh-assets-json/contents/files/assets/' + path,
			json: true,
			headers: {
				'User-Agent': 'FeH Exporter Bot'
			}
		};

		let json = await rp(options);
		let filenames = [];

		for (let i=0; i < json.length; i++) {
			filenames.push(json[i].name);
		}

		return filenames;

	}

	// Returns skills in a better readable format
	static get_skills(skill_arr: string[][]) {

		let skill_table: character_skills = {
			"weapon" : [],
			"assist" : [],
			"special" : [],
			"passive_A" : [],
			"passive_B" : [],
			"passive_C" : []
		};

		function get_SID_obj(SID: string, skill_array: skill_object[]) {
			for (let i=0; i < skill_array.length; i++) {
				if (skill_array[i].SID === SID) {
					return i;
				}
			}
			return -1;
		}

		for (let rarity=0; rarity < skill_arr.length; rarity++) {

			for (let i=0; i < skill_arr[rarity].length; i++) {

				let skill_type: "weapon" | "assist" | "special" | "passive_A" | "passive_B" | "passive_C" | null = null;
				let skill_default = false;

				switch (i) {
					case 0:
						skill_type = "weapon";
						skill_default = true;
						break;
					case 1:
						skill_type = "assist";
						skill_default = true;
						break;
					case 2:
						skill_type = "special";
						skill_default = true;
						break;
					case 6:
						skill_type = "weapon";
						break;
					case 7:
						skill_type = "assist";
						break;
					case 8:
						skill_type = "special";
						break;
					case 9:
						skill_type = "passive_A";
						break;
					case 10:
						skill_type = "passive_B";
						break;
					case 11:
						skill_type = "passive_C";
						break;
				}

				let skill_obj: skill_object = {
					"SID" : skill_arr[rarity][i],
				};

				if ( (skill_type == null) || (skill_obj.SID == null) ) {
					continue;
				}


				if (skill_default) {
					skill_obj.default = "S"+(rarity+1)
				}else{
					skill_obj.unlock = "S"+(rarity+1)
				}

				let SID_index = get_SID_obj(skill_obj.SID, skill_table[skill_type]);

				if ( SID_index >= 0) {
					skill_table[skill_type][SID_index] = Object.assign({}, skill_table[skill_type][SID_index], skill_obj);
				}else{
					skill_table[skill_type].push(skill_obj);
				}

			}

		}

		return skill_table;

	}

	async get_language_strings(language: string) {

		const ctrl = this;

		if (ctrl.languages[language][ctrl.data_file] == null) {

			await rp(assetsPath + language + '/Message/Data/Data_' + ctrl.data_file, function (error, response, body) {
				ctrl.languages[language][ctrl.data_file] = JSON.parse(body);
			});

			// This data needs to be merged
			if (ctrl.data_file === "00_first.json") {
				await rp(assetsPath + language + '/Message/Data/Data_Tutorial.json', function (error, response, body) {
					let json = JSON.parse(body);
					ctrl.languages[language][ctrl.data_file] = ctrl.languages[language][ctrl.data_file].concat(json);
				});
			}
		}

	}

	// Gets skill strings from MessageData and adds them to the collection
	async localize_skill(object: any, language: string) {

		await this.get_language_strings(language);

		let json = this.languages[language][this.data_file];
		let obj = {} as skill_strings;

		for (let i=0; i < json.length; i++) {
			switch (json[i].key) {
				case object.name_id:
					obj.name = json[i].value;
					break;
				case object.desc_id:
					obj.description = json[i].value;
					break;
			}
		}

		return obj;

	}

	// Gets character strings from MessageData and adds them to the collection
	async localize_char(id_tag: string, language: string) {

		await this.get_language_strings(language);

		let name = id_tag.substring(4);
		let json = this.languages[language][this.data_file];
		let obj = {} as character_strings;

		for (let i=0; i < json.length; i++) {
			switch (json[i].key) {
				case "MPID_"+name:
					obj.name = json[i].value;
					break;
				case "MPID_HONOR_"+name:
					obj.title = json[i].value;
					break;
				case "MPID_H_"+name:
					obj.description = json[i].value;
					break;
				case "MPID_VOICE_"+name:
					obj.voice = json[i].value;
					break;
				case "MPID_ILLUST_"+name:
					obj.artist = json[i].value;
					break;
			}
		}

		return obj;

	}

}

class FehHelper {

	constructor() {}

	/**
	 * Calculates Level 40 Stats and Superbanes/Superboons
	 * @param stats Contains stat_objects for several rarities
	 * @param growths stat_object containing stat growths
	 */
	static calcIV(stats: basestats, growths: stat_object): ivs {

		function return_stats(stats: basestats, rarity: 'S3' | 'S4' | 'S5') {

			let iv_output = {} as iv_object;

			for (const [key,] of Object.entries(stats[rarity])) {

				let k = key as 'hp' | 'atk' | 'spd' | 'def' | 'res';

				let key_low = (stats[rarity][k] - 1) + FehHelper.get_GV(growths[k] - 5, rarity);
				let key_mid = stats[rarity][k] + FehHelper.get_GV(growths[k], rarity);
				let key_high = (stats[rarity][k] + 1) + FehHelper.get_GV(growths[k] + 5, rarity);

				let key_obj: iv_stats = {
					"LV01" : stats[rarity][k],
					"LV40" : key_mid
				};

				if (rarity === "S5") {
					let bane = ((key_mid - key_low) === 4);
					let boon = ((key_high - key_mid) === 4);

					key_obj["bane"] = bane;
					key_obj["boon"] = boon;
				}

				iv_output[k] = key_obj;

			}

			return iv_output;
		}

		return {
			S5: return_stats(stats, "S5"),
			S4: return_stats(stats, "S4"),
			S3: return_stats(stats, "S3")
		};

	}

	/**
	 * Returns Basestats for all rarities
	 * @param base_stats The base stats used for the calculation
	 * @param rarity The rarity used to calculate actual bases
	 */
	static getBases(base_stats: stat_object, rarity="S5"): stat_object {

		let new_stats = [base_stats.hp, base_stats.atk, base_stats.spd, base_stats.def, base_stats.res];
		let indexes = baseIncrease(new_stats);

		new_stats[indexes[0]]++;
		new_stats[indexes[1]]++;

		if (rarity === "S5") {
			for(let i=0; i<new_stats.length; i++){
				if ((i !== indexes[0]) && (i!== indexes[1])) {
					new_stats[i]++;
				}
			}
		}

		function baseIncrease(stats: number[]) {

			let temp_stats = [...stats];
			temp_stats[0] = 0;

			let index_1 = temp_stats.findIndex(current => current === Math.max(...temp_stats));
			temp_stats[index_1] = 0;
			let index_2 = temp_stats.findIndex(current => current === Math.max(...temp_stats));

			return [index_1, index_2];

		}

		return {
			"hp" : new_stats[0],
			"atk": new_stats[1],
			"spd": new_stats[2],
			"def": new_stats[3],
			"res": new_stats[4]
		};

	}

	/**
	 * Converts move_type from number to string
	 * @param move_type the move_index to look after
	 */
	static moveString(move_type: number) {

		let move = "Infantry";

		switch(move_type) {
			case 1:
				move = "Armored";
				break;
			case 2:
				move = "Cavalry";
				break;
			case 3:
				move = "Flying";
				break;
		}

		return move;

	}

	/**
	 * Converts weapon_type from number to string
	 * @param weapon_type the weapon_index to look after
	 */
	static weaponString(weapon_type: number) {

		const weapon_index = ["Sword", "Lance", "Axe",
			"Red bow", "Blue bow", "Green bow", "Colorless bow",
			"Red Dagger", "Blue Dagger", "Green Dagger", "Colorless dagger",
			"Red Tome", "Blue Tome", "Green Tome", "Colorless Tome",
			"Staff",
			"Red Breath", "Blue Breath", "Green Breath","Colorless Breath",
			"Red Beast", "Blue Beast", "Green Beast", "Colorless Beast"
		];

		return weapon_index[weapon_type];

	}

	/**
	 * Returns the Growth Value for Level 40
	 * @param growth The growth rate for a characters stat
	 * @param rarity The characters rarity
	 */
	static get_GV(growth: number, rarity: string) {

		let rarity_factor = 1.00;

		switch (rarity) {
			case "S5":
				rarity_factor = 1.14;
				break;
			case "S4":
				rarity_factor = 1.07;
				break;
		}

		const f = (growth * rarity_factor).toFixed(2);

		let AppliedGrowthRate = Math.trunc(parseFloat(f)) / 100;

		return Math.trunc(39 * AppliedGrowthRate);
	}

}

interface mongo_connection {
	username: string;
	password: string;
	cluster: string;
	database: string;
}

export {FehExporter, FehHelper};

