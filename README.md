### First steps

This program is based on typescript. Run `npm run tsc` to generate JavaScript files.
You can then use the module normally. **If you are installing by npm this step is not necessary.**

### Using the porter

Just import the FehExporter class from the package.
```
const {FehExporter} = require('feh-db-porter');
```

You can then initialize the class with the following command:

```
const porter = new FehExporter({
username: "yourusername",
password: "yourpassword",
cluster: "yourcluster",
database: "databasetouse"
})
````

Then use `porter.compile()` whenever you want to import Fire Emblem Heroes data into your MongoDB.
The programm uses update methods with upsert so it will only insert new records if
the record doesn't exist.

The data is collected from HertzDevils FeH repository
which is also being used by the Fire Emblem Heroes wiki on Gamepedia.
